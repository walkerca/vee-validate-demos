import { createRouter, createWebHistory } from 'vue-router'
import ContactInfo_ComponentFramework from "@/views/Component_Wrapper/ContactInfo_ComponentFramework.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: ContactInfo_ComponentFramework
    },
    {
      path: '/ext',
      name: 'ext',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/Component_Wrapper/ContactInfo_ComponentFramework_Ext.vue')
    },
    {
      path: '/ext2',
      name: 'ext2',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/Component_Wrapper/ContactInfo_ComponentFramework_Ext_2.vue')
    },
    {
      path: '/riv1',
      name: 'riv1',
      component: () => import('../views/Reactive_Initial_Values/InitialValuesAtSetup.vue')
    },
    {
      path: '/riv2',
      name: 'riv2',
      component: () => import('../views/Reactive_Initial_Values/BrokenInitialValuesOnMounted.vue')
    },
    {
      path: '/riv3',
      name: 'riv3',
      component: () => import('../views/Reactive_Initial_Values/DeferredOnMounted.vue')
    },
    {
      path: '/riv4',
      name: 'riv4',
      component: () => import('../views/Reactive_Initial_Values/CompositionAPI.vue')
    },
    {
      path: '/cvf',
      name: 'cvf',
      component: () => import('@/views/Crossfield_Validation/CFV_Page.vue')
    }
  ]
})

export default router
