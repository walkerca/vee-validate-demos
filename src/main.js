import {createApp} from 'vue'
import {createPinia} from 'pinia'

import App from './App.vue'
import router from './router'
import {defineRule} from "vee-validate";
import {max, required} from '@vee-validate/rules';

defineRule('required',required)
defineRule('max', max)

const app = createApp(App)

app.use(createPinia())
app.use(router)
app.mount('#app')
